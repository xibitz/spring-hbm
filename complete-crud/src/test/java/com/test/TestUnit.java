package com.test;

import com.websystique.springboot.SpringBootCRUDApp;
import com.websystique.springboot.model.User;
import com.websystique.springboot.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = TestBackendApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest(classes = SpringBootCRUDApp.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class TestUnit {

    private static Logger logger = LoggerFactory.getLogger("UNIT_TESTING");

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testingData() {
        System.out.println("test insert user");
        User user = new User();
        user.setName("sample");
        user.setAge(21);
        user.setSalary(20000);

        userRepository.save(user);

        System.out.println("user inserted, with id : "+user.getId());

    }
}
