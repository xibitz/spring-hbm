package com.training.hibernate;

import com.training.hibernate.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        // create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .addAnnotatedClass(Course.class)
                .addAnnotatedClass(Review.class)
                .addAnnotatedClass(Student.class)
                .buildSessionFactory();

        // create session
        Session session = factory.getCurrentSession();

        try {
            // create the objects
            Instructor tempInstructor =
                    new Instructor("Yasho", "Maladhi", "yasho@maladhi.com");

            InstructorDetail tempInstructorDetail =
                    new InstructorDetail(
                            "http://www.youtube.com",
                            "Reading");

            // associate the objects
            tempInstructor.setInstructorDetail(tempInstructorDetail);

            Course cou = new Course("Trainer");
            cou.setInstructor(tempInstructor);
            Course cou2 = new Course("Software Architect");
            cou2.setInstructor(tempInstructor);

            List<Course> courseList = new ArrayList<>();
            courseList.add(cou);
            courseList.add(cou2);

            tempInstructor.setCourses(courseList);

            // start a transaction
            session.beginTransaction();

            // save the instructor
            //
            // Note: this will ALSO save the details object
            // because of CascadeType.ALL
            //
            System.out.println("Saving instructor: " + tempInstructor);
            session.save(tempInstructor);

            // commit transaction
            session.getTransaction().commit();

            System.out.println("Done!");
        }
        finally {
            factory.close();
        }
    }
}
